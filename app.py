from flask import Flask, render_template
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/data")
def get_data():
    return {
        "data": [
            [
                {"color": "white", "key": 20, "note": "C"},
                {"color": "black", "key": 81, "note": "^C"},
            ],
            [
                {"color": "white", "key": 65, "note": "D"},
                {"color": "black", "key": 87, "note": "^D"},
            ],
            [{"color": "white", "key": 83, "note": "E"}],
            [
                {"color": "white", "key": 68, "note": "F"},
                {"color": "black", "key": 82, "note": "^F"},
            ],
            [
                {"color": "white", "key": 70, "note": "G"},
                {"color": "black", "key": 84, "note": "^G"},
            ],
            [
                {"color": "white", "key": 71, "note": "A"},
                {"color": "black", "key": 89, "note": "^A"},
            ],
            [{"color": "white", "key": 72, "note": "B"}],
            [
                {"color": "white", "key": 74, "note": "c"},
                {"color": "black", "key": 73, "note": "^c"},
            ],
            [
                {"color": "white", "key": 75, "note": "d"},
                {"color": "black", "key": 79, "note": "^d"},
            ],
            [{"color": "white", "key": 76, "note": "e"}],
            [
                {"color": "white", "key": 186, "note": "f"},
                {"color": "black", "key": 219, "note": "^f"},
            ],
            [
                {"color": "white", "key": 222, "note": "g"},
                {"color": "black", "key": 221, "note": "^g"},
            ],
            [
                {"color": "white", "key": 220, "note": "a"},
                {"color": "black", "key": 13, "note": "^a"},
            ],
            [{"color": "white", "key": 37, "note": "b"}],
        ]
    }
