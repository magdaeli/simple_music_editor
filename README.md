# Simple Music Editor

- [Projektbeschreibung](#projektbeschreibung)  
  - [Backend](#backend)  
  - [Frontend](#frontend)  
  - [Begründung](#begründung)  
- [Setup project](#setup-project)  
- [Team](#team)

## Projektbeschreibung

Der Simple Music Editor ist eine Browseranwendung zur Notation einfacher Melodien im Violinschlüssel. Nachdem der Nutzer die Tonhöhe ausgewählt hat, kann er in einem zweiten Schritt den Notenwert festlegen. Eingaben können mittels des entsprechenden Buttons rückgängig gemacht werden.

### Backend

Für das **Backend** wurde das Python-Microframework [Flask](https://flask.palletsprojects.com/en/3.0.x/) verwendet; einer der zwei vorhandenen Endpunkte (/) liefert die index.html aus, der andere (/data) stellt die Daten zur Erzeugung der Tastatur zur Verfügung.

### Frontend

Im **Frontend** wird das JavaScript-Framework [Vue.js](https://vuejs.org/) genutzt sowie [abcjs](https://www.abcjs.net/), um die Musiknotation zu rendern.

### Begründung

Der Fokus der Anwendung liegt ganz klar auf der Interaktivität im Browser, weshalb nur ein geringer Python-Anteil für die Programmierung notwendig war (der unter anderen Voraussetzungen sicher noch weiter hätte reduziert werden können). Nichtsdestotrotz bietet die Anwendung auf diese Weise viele Anknüpfungspunkte für potentielle Erweiterungen (z.B. Tonwiedergabe bei Tastendruck, MIDI-Erzeugung der Melodie etc.).

## Setup project
Tested with Ubuntu 22.04.3 LTS and Firefox 121.0.1

- Clone git repository:
  - `git clone https://codeberg.org/magdaeli/simple_music_editor.git`
- Change to project folder:
  - `cd simple_music_editor`



- Create virtual environment (only once):
  - `python3 -m venv .venv`



- Activate virtual environment:
  - `. .venv/bin/activate`

- Install dependencies:
  - `pip install -r requirements.txt`

- Run app locally:
  - `flask run` (resp. `flask run --debug` for development)



- Deactivate virtual environment:
  - `deactivate`


## Team

- Marius Wagner
- Magdalena Büttner

Dank an Dennis Bölling für die Einführung in Vue.js